<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210318204014 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Ajout des produits emballage';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (664.09, 12, 0, 0)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (984.87, 12, 0, 0)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (770.66, 9, 1, 0)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1138.15, 35, 1, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1336.14, 35, 1, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1880.97, 16, 1, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1936.04, 16, 2, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1160.25, 35, 1, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1258.96, 12, 3, 0)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1397.47, 12, 1, 0)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1253.33, 35, 1, 1)');
        $this->addSql('INSERT INTO ProduitEmballage (prixVente, countPapier, countRuban, countEtiquette) VALUES (1609.01, 16, 2, 0)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
