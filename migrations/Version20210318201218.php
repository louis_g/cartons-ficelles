<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210318201218 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Rajout des différents matériaux';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO materiel (name, prix) VALUES ("carton", 0.40)');
        $this->addSql('INSERT INTO materiel (name, prix) VALUES ("papier", 0.20)');
        $this->addSql('INSERT INTO materiel (name, prix) VALUES ("ficelle", 0.48)');
        $this->addSql('INSERT INTO materiel (name, prix) VALUES ("ruban", 0.40)');
        $this->addSql('INSERT INTO materiel (name, prix) VALUES ("etiquette", 0.40)');
    }

    public function down(Schema $schema) : void
    {

    }
}
