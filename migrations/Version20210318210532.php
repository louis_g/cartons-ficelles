<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210318210532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Ajout des produits carton ficelle';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (1407.66, 10, 5)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2050.28, 12, 20)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (1402.55, 13, 10)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (1382.26, 13, 10)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2080.49, 13, 10)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2133.32, 13, 10)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2728.76, 16, 20)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2065.28, 16, 20)');
        $this->addSql('INSERT INTO ProduitCartonFicelle (prixVente, countCarton, countFicelle) VALUES (2769.13, 17, 10)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
