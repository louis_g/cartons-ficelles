-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : sam. 27 mars 2021 à 00:59
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projet_php`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210318194906', '2021-03-18 20:49:44', 392),
('DoctrineMigrations\\Version20210318195954', '2021-03-18 21:00:05', 155),
('DoctrineMigrations\\Version20210318200953', '2021-03-18 21:10:23', 75),
('DoctrineMigrations\\Version20210318201218', '2021-03-18 21:26:38', 54),
('DoctrineMigrations\\Version20210318204014', '2021-03-18 21:59:49', 56),
('DoctrineMigrations\\Version20210318210532', '2021-03-18 22:22:59', 49);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

CREATE TABLE `materiel` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `materiel`
--

INSERT INTO `materiel` (`id`, `name`, `prix`) VALUES
(1, 'carton', 0.4),
(2, 'papier', 0.2),
(3, 'ficelle', 0.48),
(4, 'ruban', 0.4),
(5, 'etiquette', 0.4);

-- --------------------------------------------------------

--
-- Structure de la table `produitcartonficelle`
--

CREATE TABLE `produitcartonficelle` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Un nom',
  `prixVente` float NOT NULL,
  `countCarton` int(255) NOT NULL,
  `countFicelle` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produitcartonficelle`
--

INSERT INTO `produitcartonficelle` (`id`, `name`, `prixVente`, `countCarton`, `countFicelle`) VALUES
(1, 'Cartons et bouts de ficelle 1', 1407.66, 10, 5),
(2, 'Cartons et bouts de ficelle 2', 2050.28, 12, 20),
(3, 'Cartons et bouts de ficelle 3', 1402.55, 13, 10),
(4, 'Cartons et bouts de ficelle 4', 1382.26, 13, 10),
(5, 'Cartons et bouts de ficelle 5', 2080.49, 13, 10),
(6, 'Cartons et bouts de ficelle 6', 2133.32, 13, 10),
(7, 'Cartons et bouts de ficelle 7', 2728.76, 16, 20),
(8, 'Cartons et bouts de ficelle 8', 2065.28, 16, 20),
(9, 'Cartons et bouts de ficelle 9', 2769.13, 17, 10);

-- --------------------------------------------------------

--
-- Structure de la table `produitemballage`
--

CREATE TABLE `produitemballage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Un nom',
  `prixVente` float NOT NULL,
  `countPapier` int(255) NOT NULL,
  `countRuban` int(255) NOT NULL,
  `countEtiquette` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produitemballage`
--

INSERT INTO `produitemballage` (`id`, `name`, `prixVente`, `countPapier`, `countRuban`, `countEtiquette`) VALUES
(1, 'Emballage 1', 664.09, 12, 1, 0),
(2, 'Emballage 2', 984.87, 12, 0, 0),
(3, 'Emballage 3', 770.66, 9, 1, 0),
(4, 'Emballage 4', 1138.15, 35, 1, 1),
(5, 'Emballage 5', 1336.14, 35, 1, 1),
(6, 'Emballage 6', 1880.97, 16, 1, 1),
(7, 'Emballage 7', 1936.04, 16, 2, 1),
(8, 'Emballage 8', 1160.25, 35, 1, 1),
(9, 'Emballage 9', 1258.96, 12, 3, 0),
(10, 'Emballage 10', 1397.47, 12, 1, 0),
(11, 'Emballage 11', 1253.33, 35, 1, 1),
(12, 'Emballage 12', 1609.01, 16, 2, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produitcartonficelle`
--
ALTER TABLE `produitcartonficelle`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produitemballage`
--
ALTER TABLE `produitemballage`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `materiel`
--
ALTER TABLE `materiel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `produitcartonficelle`
--
ALTER TABLE `produitcartonficelle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `produitemballage`
--
ALTER TABLE `produitemballage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
