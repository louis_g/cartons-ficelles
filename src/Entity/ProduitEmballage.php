<?php

namespace App\Entity;

use App\Repository\ProduitEmballageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitEmballageRepository::class)
 * @ORM\Table(name="produitemballage")
 */
class ProduitEmballage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @ORM\Column(type="float", name="prixVente")
     */
    private $prixVente;

    /**
     * @ORM\Column(type="integer", name="countPapier")
     */
    private $countPapier;

    /**
     * @ORM\Column(type="integer", name="countRuban")
     */
    private $countRuban;

    /**
     * @ORM\Column(type="integer", name="countEtiquette")
     */
    private $countEtiquette;

    private $minPricePapier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrixVente(): ?float
    {
        return $this->prixVente;
    }

    public function setPrixVente(float $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getCountPapier(): ?int
    {
        return $this->countPapier;
    }

    public function setCountPapier(int $countPapier): self
    {
        $this->countPapier = $countPapier;

        return $this;
    }

    public function getCountRuban(): ?int
    {
        return $this->countRuban;
    }

    public function setCountRuban(int $countRuban): self
    {
        $this->countRuban = $countRuban;

        return $this;
    }

    public function getCountEtiquette(): ?int
    {
        return $this->countEtiquette;
    }

    public function setCountEtiquette(int $countEtiquette): self
    {
        $this->countEtiquette = $countEtiquette;

        return $this;
    }

    public function getMinPricePapier(): ?float
    {
        return $this->minPricePapier;
    }

    public function setMinPricePapier(float $minPricePapier): self
    {
        $this->minPricePapier = $minPricePapier;

        return $this;
    }
}
