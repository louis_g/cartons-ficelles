<?php

namespace App\Entity;

use App\Repository\ProduitCartonFicelleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitCartonFicelleRepository::class)
 * @ORM\Table(name="produitcartonficelle")
 */
class ProduitCartonFicelle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @ORM\Column(type="float", name="prixVente")
     */
    private $prixVente;

    /**
     * @ORM\Column(type="integer", name="countCarton")
     */
    private $countCarton;

    /**
     * @ORM\Column(type="integer", name="countFicelle")
     */
    private $countFicelle;

    private $minPriceCarton;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrixVente(): ?float
    {
        return $this->prixVente;
    }

    public function setPrixVente(float $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getCountCarton(): ?int
    {
        return $this->countCarton;
    }

    public function setCountCarton(int $countCarton): self
    {
        $this->countCarton = $countCarton;

        return $this;
    }

    public function getCountFicelle(): ?int
    {
        return $this->countFicelle;
    }

    public function setCountFicelle(int $countFicelle): self
    {
        $this->countFicelle = $countFicelle;

        return $this;
    }

    public function getMinPriceCarton(): ?float
    {
        return $this->minPriceCarton;
    }

    public function setMinPriceCarton(float $minPriceCarton): self
    {
        $this->minPriceCarton = $minPriceCarton;

        return $this;
    }


}
