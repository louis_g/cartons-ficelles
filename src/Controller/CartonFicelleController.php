<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Entity\ProduitCartonFicelle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartonFicelleController extends AbstractController
{
    /**
     * @Route("/carton_ficelle", name="carton_ficelle")
     */
    public function index(): Response
    {
        $res = $this->calculateAchatBenefice();
        return $this->render('carton_ficelle/index.html.twig', [
            'controller_name' => 'CartonFicelleController',
            'carton_ficelle' => $res['carton_ficelle'],
            'carton' => $res['carton'],
            'ficelle' => $res['ficelle'],
            'tri' => false,
            'currentPage' =>'carton_ficelle'
        ]);
    }

    /**
     * @Route("/calculatecf/{prix}", name="calculatecf", methods={"GET"})
     * @param float $prix
     * @return Response
     */
    public function calculateRoute(float $prix): Response
    {
        try{
            return $this->json($this->calculateAchatBenefice($prix));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function calculateAchatBenefice($prixCarton = null) {
        $carton_ficelle = $this->getDoctrine()->getRepository(ProduitCartonFicelle::class)->findAll();
        $carton = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'carton'))[0];
        $ficelle = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ficelle'))[0];
        if($prixCarton == null){
            $prixCarton = $carton->getPrix();
        }
        foreach ($carton_ficelle as $el){
            $prixAchat = $carton->getPrix() * $el->getCountCarton() + $ficelle->getPrix() * $el->getCountFicelle();
            $el->prixAchat = $prixAchat;
            $el->benefices = $el->getPrixVente() - $prixAchat;
        }
        return [
            'carton_ficelle' => $carton_ficelle,
            'carton' => $carton->getPrix(),
            'ficelle' => $ficelle->getPrix(),
        ];
    }
}
