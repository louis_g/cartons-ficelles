<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SignalerBugController extends AbstractController
{
    /**
     * @Route("/signaler/bug", name="signaler_bug")
     */
    public function index(): Response
    {
        return $this->render('signaler_bug/index.html.twig', [
            'controller_name' => 'SignalerBugController',
        ]);
    }
}
