<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Entity\ProduitCartonFicelle;
use App\Entity\ProduitEmballage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfigurationController extends AbstractController
{
    /**
     * @Route("/configuration", name="configuration")
     */
    public function index(): Response
    {
        $res = $this->getInfos();
        return $this->render('configuration/index.html.twig', [
            'controller_name' => 'ConfigurationController',
            'emballages' => $res['emballages'],
            'carton_ficelle' => $res['carton_ficelle'],
            'ruban' => $res['ruban'],
            'etiquette' => $res['etiquette'],
            'ficelle' => $res['ficelle'],
        ]);
    }

    public function getInfos() {
        $carton_ficelle = $this->getDoctrine()->getRepository(ProduitCartonFicelle::class)->findAll();
        $emballages = $this->getDoctrine()->getRepository(ProduitEmballage::class)->findAll();

        $ruban = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ruban'))[0];;
        $etiquette = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'etiquette'))[0];;
        $ficelle = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ficelle'))[0];

        return [
            'ruban' => $ruban->getPrix(),
            'etiquette' => $etiquette->getPrix(),
            'ficelle' => $ficelle->getPrix(),
            'carton_ficelle' => $carton_ficelle,
            'emballages' => $emballages,
        ];
    }
    
    /**
     * @Route("/changePrice/{ruban}/{etiquette}/{ficelle}", name="changePrice", methods={"GET"})
     * @param float $ruban, $etiquette, $ficelle
     * @return Response
     */
    public function changePrice(float $ruban, float $etiquette, float $ficelle): Response
    {
        try{
            return $this->json($this->setInfos($ruban,$etiquette,$ficelle));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function setInfos($_ruban ,$_etiquette, $_ficelle)
    {
        $entitymanager = $this->getDoctrine()->getManager();

        $ficelle = $entitymanager->getRepository(Materiel::class)->find(3);
        $ficelle->setPrix($_ficelle);

        $ruban = $entitymanager->getRepository(Materiel::class)->find(4);
        $ruban->setPrix($_ruban);

        $etiquette = $entitymanager->getRepository(Materiel::class)->find(5);
        $etiquette->setPrix($_etiquette);

        $entitymanager->flush();

    }

    /**
     * @Route("/changeComposition/{id}/{papier}/{ruban}/{etiquette}", name="changeComposition", methods={"GET"})
     * @param float $id, $papier, $ruban, $etiquette
     * @return Response
     */
    public function changeComposition(int $id ,float $papier, float $ruban, float $etiquette): Response
    {
        try{
            return $this->json($this->setComposition($id, $papier, $ruban,$etiquette));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function setComposition($id , $_papier, $_ruban ,$_etiquette )
    {
        $entitymanager = $this->getDoctrine()->getManager();

        $emballage = $entitymanager->getRepository(ProduitEmballage::class)->find($id);
        $emballage->setCountRuban($_ruban);
        $emballage->setCountEtiquette($_etiquette);
        $emballage->setCountPapier($_papier);

        $entitymanager->flush();
    }

    /**
     * @Route("/changeCompositioncf/{id}/{carton}/{ficelle}", name="changeCompositioncf", methods={"GET"})
     * @param float $id, $carton, $ficelle
     * @return Response
     */
    public function changeCompositioncf(int $id ,float $carton, float $ficelle): Response
    {
        try{
            return $this->json($this->setCompositioncf($id, $carton, $ficelle));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function setCompositioncf($id , $_carton, $_ficelle  )
    {
        $entitymanager = $this->getDoctrine()->getManager();

        $emballage = $entitymanager->getRepository(ProduitCartonFicelle::class)->find($id);
        $emballage->setCountCarton($_carton);
        $emballage->setCountFicelle($_ficelle);

        $entitymanager->flush();
    }
}
