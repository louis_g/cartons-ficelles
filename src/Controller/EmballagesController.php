<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Entity\ProduitCartonFicelle;
use App\Entity\ProduitEmballage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmballagesController extends AbstractController
{
    /**
     * @Route("/emballages", name="emballages")
     */
    public function index(): Response
    {
        $res = $this->calculateAchatBenefice();
        return $this->render('emballages/index.html.twig', [
            'controller_name' => 'EmballagesController',
            'emballages' => $res['emballages'],
            'etiquette' => $res['etiquette'],
            'ruban' => $res['ruban'],
            'papier' => $res['papier'],
            'currentPage' =>'emballage'
        ]);
    }

    /**
     * @Route("/calculate/{prix}", name="calculate", methods={"GET"})
     * @param float $prix
     * @return Response
     */
    public function calculateRoute(float $prix): Response
    {
        try{
            return $this->json($this->calculateAchatBenefice($prix));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function calculateAchatBenefice($prixPapier = null) {
        $emballages = $this->getDoctrine()->getRepository(ProduitEmballage::class)->findAll();
        $etiquette = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'etiquette'))[0];
        $ruban = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ruban'))[0];
        $papier = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'papier'))[0];
        if($prixPapier == null){
            $prixPapier = $papier->getPrix();
        }
        foreach ($emballages as $el){
            $prixAchat = $etiquette->getPrix() * $el->getCountEtiquette() + $ruban->getPrix() * $el->getCountRuban()
                + $prixPapier * $el->getCountPapier();
            $el->prixAchat = $prixAchat;
            $el->benefices = $el->getPrixVente() - $prixAchat;
        }
        return [
            'emballages' => $emballages,
            'etiquette' => $etiquette->getPrix(),
            'ruban' => $ruban->getPrix(),
            'papier' => $prixPapier
        ];
    }

    /**
     * @Route("/rentab", name="rentab")
     */
    public function rentab(): Response {
        $emballages = $this->getDoctrine()->getRepository(ProduitEmballage::class)->findAll();
        $cartonsFicelles = $this->getDoctrine()->getRepository(ProduitCartonFicelle::class)->findAll();
        $etiquette = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'etiquette'))[0];
        $ruban = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ruban'))[0];
        $papier = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'papier'))[0];
        $carton = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'carton'))[0];
        $ficelle = $this->getDoctrine()->getRepository(Materiel::class)->findBy(array('name' => 'ficelle'))[0];

        for ($i = 0; $i < count($cartonsFicelles); $i++){
            $prixSansCarton = $cartonsFicelles[$i]->getPrixVente() - ($ficelle->getPrix() * $cartonsFicelles[$i]->getCountFicelle());
            $prixSansCarton = $prixSansCarton / $cartonsFicelles[$i]->getCountCarton();
            $cartonsFicelles[$i]->setMinPriceCarton(round($prixSansCarton, 1));
        }

        for ($i = 0; $i < count($emballages); $i++){
            $prixSansPapier = $emballages[$i]->getPrixVente()
                - ($ruban->getPrix() * $emballages[$i]->getCountRuban())
                - ($etiquette->getPrix() * $emballages[$i]->getCountEtiquette());
            $prixSansPapier = $prixSansPapier / $emballages[$i]->getCountPapier();
            $emballages[$i]->setMinPricePapier(round($prixSansPapier, 1));
        }

        return $this->render('rentab/index.html.twig', [
            'controller_name' => 'EmballagesController',
            'emballages' => $emballages,
            'cartonFicelle' => $cartonsFicelles,
            'etiquette' => $etiquette->getPrix(),
            'ruban' => $ruban->getPrix(),
            'papier' => $papier->getPrix(),
            'carton' => $carton->getPrix(),
            'ficelle' => $ficelle->getPrix()
        ]);
    }
}
