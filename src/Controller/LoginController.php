<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(): Response
    {
        return $this->render('login/index.html.twig', [
            'controller_name' => 'LoginController',
            'currentPage' =>'carton_ficelle'
        ]);
    }

    /**
     * @Route("/authenticate/{password}", name="authenticate" , methods={"GET"})
     * @param string $password
     * @return Response
     */
    public function login(string $password): Response 
    {
        try{
            return $this->json($this->verifyPassword($password));
        }catch (Exception $err){
            return $this->json($err);
        }
    }

    public function verifyPassword($password = null)
    {
        if ($password == "admin")
            return true;
        else
            return false;
    }
}
