<?php

namespace App\Repository;

use App\Entity\ProduitCartonFicelle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProduitCartonFicelle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProduitCartonFicelle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProduitCartonFicelle[]    findAll()
 * @method ProduitCartonFicelle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitCartonFicelleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProduitCartonFicelle::class);
    }

    // /**
    //  * @return ProduitCartonFicelle[] Returns an array of ProduitCartonFicelle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProduitCartonFicelle
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
