<?php

namespace App\Repository;

use App\Entity\ProduitEmballage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProduitEmballage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProduitEmballage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProduitEmballage[]    findAll()
 * @method ProduitEmballage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitEmballageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProduitEmballage::class);
    }

    // /**
    //  * @return ProduitEmballage[] Returns an array of ProduitEmballage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProduitEmballage
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
